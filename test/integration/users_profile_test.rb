require 'test_helper'

class UsersProfileTest < ActionDispatch::IntegrationTest
  include ApplicationHelper
  # test "the truth" do
  #   assert true
  # end

  def setup
    @user = users(:mykola)
  end

  test "profile display" do
    get user_path(@user)
    assert_template 'users/show'
    assert_select 'title', full_title(@user.name)
    assert_select 'h1', text: @user.name
    assert_select 'h1>img.gravatar'
    assert_match @user.tasks.count.to_s, response.body
    assert_select 'div.pagination'
    @user.tasks.paginate(page: 1, per_page: 5).each do |task|
      assert_match task.content, response.body
    end
  end
end
