require 'test_helper'

class TaskTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  def setup
    @user = users(:mykola)
    @task = @user.tasks.build(content: "First task")
  end

  test "should be valid" do
    assert @task.valid?
  end

  test "user id should be present" do
    @task.user_id = nil
    assert_not @task.valid?
  end

  test "content should be present" do
    @task.content = "  "
    assert_not @task.valid?
  end

  test "content length should be no longer than 140 characters" do
    @task.content = "a"*141
    assert_not @task.valid?
  end

  test "order should be most recent first" do
    assert_equal Task.first, tasks(:most_recent)
  end
end
